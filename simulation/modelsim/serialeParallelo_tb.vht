-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "11/15/2018 14:19:02"
                                                            
-- Vhdl Test Bench template for design  :  serialeParallelo
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY serialeParallelo_tb_vhd_tst IS
END serialeParallelo_tb_vhd_tst;
ARCHITECTURE serialeParallelo_arch OF serialeParallelo_tb_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC := '0';
SIGNAL data : STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";
SIGNAL ready : STD_LOGIC := '0';
SIGNAL reset : STD_LOGIC := '0';
SIGNAL serial : STD_LOGIC := '1';
SIGNAL period :time := 20 ns;
COMPONENT serialeParallelo
	PORT (
	clk : IN STD_LOGIC;
	data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	ready : OUT STD_LOGIC;
	reset : IN STD_LOGIC;
	serial : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : serialeParallelo
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	data => data,
	ready => ready,
	reset => reset,
	serial => serial
	);
clock : PROCESS                                                                                    
BEGIN
	clk <= '1';
	WAIT FOR period/2;
	clk <= '0';
	WAIT FOR period/2;
                                                       
END PROCESS clock;                                           

always : PROCESS                                                                                    
BEGIN                                                         
        serial <= '1', '0' AFTER 2*period, '1' AFTER 3*period, '0' AFTER 4*period, '1' AFTER 7*period, '1' AFTER 9*period;
		  
WAIT FOR 20 * period;                                                     
END PROCESS always;                                         
END serialeParallelo_arch;
