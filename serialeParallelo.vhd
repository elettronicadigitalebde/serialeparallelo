LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY serialeParallelo IS 
	PORT(serial :in  std_logic; --serial deve cambiare valore sul fronte di salita del clock
		  clk    :in  std_logic; 
		  reset  :in  std_logic;
		  ready  :out std_logic;
		  data   :out std_logic_vector(7 DOWNTO 0));
END;


ARCHITECTURE arch_serialeParallelo OF serialeParallelo IS

	TYPE stato is (attesa, bit7, bit6, bit5, bit4, bit3, bit2, bit1, bit0, stop, pronto, inizio);
	SIGNAL currentState, nextState :stato;
	SIGNAL temporaryData : std_logic_vector(7 DOWNTO 0);
	
	BEGIN
		
		PROCESS(serial, currentState)
			BEGIN
				CASE currentState IS
					
					WHEN attesa => IF serial = '1' THEN nextState <= attesa;
										ELSE nextState <= inizio;
										END IF;
										
					WHEN inizio => nextState <= bit7;
										
					WHEN bit7 => nextState <= bit6;
										
					WHEN bit6 => nextState <= bit5;
				
					WHEN bit5 => nextState <= bit4;
					
					WHEN bit4 => nextState <= bit3;
					
					WHEN bit3 => nextState <= bit2;
					
					WHEN bit2 => nextState <= bit1;
					
					WHEN bit1 => nextState <= bit0;
					
					WHEN bit0 => nextState <= stop;
					
					WHEN stop => nextState <= pronto;
					
					WHEN pronto => nextState <= attesa;
			
			 END CASE;
			END PROCESS;
	
	PROCESS(clk, reset)
		BEGIN
			
			IF (rising_edge(clk)) THEN
				IF reset = '0' THEN currentState <= nextState;
				ELSE currentState <= attesa;
				END IF;
			END IF;
			
	END PROCESS;
	
	PROCESS(clk, currentState)
	BEGIN
		CASE currentState IS
			WHEN attesa => ready <= '0';
			
			WHEN inizio => ready <= '0';
					
			WHEN bit7  => IF falling_edge(clk) then temporaryData(7) <= serial;
								END IF;
								
			WHEN bit6  => IF falling_edge(clk) then temporaryData(6) <= serial;
								END IF;
								
			WHEN bit5  => IF falling_edge(clk) then temporaryData(5) <= serial;
								END IF;
								
			WHEN bit4  => IF falling_edge(clk) then temporaryData(4) <= serial;
								END IF;
								
			WHEN bit3  => IF falling_edge(clk) then temporaryData(3) <= serial;
								END IF;
								
			WHEN bit2  => IF falling_edge(clk) then temporaryData(2) <= serial;
								END IF;
								
			WHEN bit1  => IF falling_edge(clk) then temporaryData(1) <= serial;
								END IF;
								
			WHEN bit0  => IF falling_edge(clk) then temporaryData(0) <= serial;
								END IF;
								
			WHEN stop => ready <= '0';					
			
			WHEN pronto => ready <= '1';
								data <= temporaryData;
		END CASE;
	END PROCESS;
	
END;